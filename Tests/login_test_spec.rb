require 'rspec'
require 'selenium-webdriver'

describe 'Test Login To App' do

  def
    element_finder(selector, attribute)
    @driver.find_element(selector, attribute)
  end

  before(:all) do
    # create an instance of driver
    @driver = Selenium::WebDriver.for (:chrome)
    # navigate to app
    @driver.get 'https://core.futuresimple.com/users/login'
    end

  after(:all) do
    @driver.quit

  end

  it 'should login, create deal, type notes, check added notes ' do
    # login
    element_finder(:id, 'user_email').send_keys 'serezha89@bigmir.net'
    element_finder(:id, 'user_password').send_keys 'Pa$$w0rd'
    element_finder(:tag_name, 'button').click

    # wait on element appearance
    @wait = Selenium::WebDriver::Wait.new(:timeout => 30) # seconds
    @wait.until {element_finder(:id, 'nav-leads').displayed?}
    element_finder(:id, 'nav-leads').click

    # populate mandatory fields
    @wait.until {element_finder(:xpath, "//*[@class='btn btn-info unselected']")}
    element_finder(:xpath, "//*[@class='btn btn-info unselected']").click
    element_finder(:id, 'lead-first-name').send_key 'test_first_name'
    element_finder(:id, 'lead-last-name').send_key 'test_last_name'
    element_finder(:id, 'lead-company-name').send_key 'test_company'
    @status_dropdown =  element_finder(:name, 'status_id')
    @select_status_list = Selenium::WebDriver::Support::Select.new(@status_dropdown)
    @select_status_list.select_by(:text, 'New')
    element_finder(:id, 'lead-email').send_key 'test@test.com'
    element_finder(:xpath, '//button[@class="save btn btn-large btn-primary"]').click

    # populate notes field
    @wait.until {element_finder(:css, 'textarea[class="span12"]').displayed?}
    element_finder(:css, 'textarea[class="span12"]').send_key 'text note about test lead'
    element_finder(:css, 'button[class="btn btn-inverse hide"]').click

    @notes = @driver.find_elements(:css, "p[class='activity-content note-content']")
    @not_nil_notes = []

    @notes.each { |x|
      if x.text != nil
        @not_nil_notes
      else
        next
      end}

    @not_nil_notes.each { |x|
      expect(x).to eql ('text note about test lead')}
  end
end

